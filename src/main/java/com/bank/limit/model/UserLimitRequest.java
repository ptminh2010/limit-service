package com.bank.limit.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class UserLimitRequest {
    @Schema(
            name = "user",
            title = "user",
            example = "user_1"
    )
    private String user;

    @Schema(
            name = "date",
            title = "date",
            example = "2023-07-17T17:48:32.460Z"
    )
    private Date date;

    @Schema(
            name = "productCode",
            title = "productCode",
            example = "INTERNAL_TRANSFER"
    )
    private String productCode;
}
