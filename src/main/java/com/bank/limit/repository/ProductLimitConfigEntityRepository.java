package com.bank.limit.repository;

import com.bank.limit.entity.ProductLimitConfigEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ProductLimitConfigEntityRepository extends JpaRepository<ProductLimitConfigEntity, UUID> {
    Optional<ProductLimitConfigEntity> findProductLimitConfigEntityByProductCode(String productCode);
}